<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $id_no
 * @property string $titulo_no
 * @property string $texto_no
 * @property string $foto_no
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo_no', 'texto_no', 'foto_no'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_no' => 'Id No',
            'titulo_no' => 'Titulo Noticia',
            'texto_no' => 'Texto No',
            'foto_no' => 'Foto No',
        ];
    }
}
