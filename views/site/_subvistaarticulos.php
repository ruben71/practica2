<?php
use yii\helpers\Html;
?>
<div class="col-sm-12 col-md-4 flex-grow">
    <div class="thumbnail">
      <div class="caption">
        <figure>
                        <?=Html::img("@web/imgs/$clavemodelo->foto_ar",[
                    'class'=>'img-responsive',
                    'style'=>[
                        'width'=>'550px',
                        'height'=>'250px',
                    ],
                    ]); ?>

        </figure>
        
        <ul>
            <li><h3>Esto sería el tituo: <?= $clavemodelo->titulo_ar; ?></h3></li>           
        </ul>
        
        <h4>Esto sería el texto corto: <?= $clavemodelo->textocorto_ar;?></h4>

    <p>
        <?= Html::a('Leer más', ['artgrande',"id"=>$clavemodelo->id_ar], ['class' => 'btn btn-success']) ?>
    </p>
      </div>
    </div>
  </div>

