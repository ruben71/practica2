<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Noticias;
use app\models\Articulos;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionNoticias()
    { 
        return $this -> render("vistanoticias",[
            "clavenoticia"=>Noticias::find()->all(),
        ]);
    }
    public function actionArticulos()
    { 
        return $this -> render("vistaarticulos",[
            "clavearticulo"=>Articulos::find()->all(),
        ]);
    }
    
    public function actionListartodo()
    {
        $dataProvidern= new ActiveDataProvider([
            'query' => Noticias::find(),
            'pagination'=>[
                'pageSize'=>1,
                'pageParam'=>'noticias',
            ]
        ]);
        $dataProvidera = new ActiveDataProvider([
            'query' => Articulos::find(),
                        'pagination'=>[
                'pageSize'=>2,
                'pageParam'=>'articulos',
            ]
        ]);
        
        return $this->render('listartodo', [
            'dataProvidern' => $dataProvidern,
            'dataProvidera' => $dataProvidera,
        ]);
    }
    
    public function actionArtgrande($id){
        $model = Articulos::findOne($id);

        return $this->render('vistaartgrande', [
            'model' => $model,
        ]);

    }
}
